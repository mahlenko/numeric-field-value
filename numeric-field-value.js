var FieldNumber = {
    Init: function(obj)
    {
        $(obj).on('keydown', function(){
            return FieldNumber.BanKeys(event);
        }).on('keyup', function(){
            FieldNumber.Check(this);
        }).change(function(){
            FieldNumber.Check(this);
        });
    },

    BanKeys: function(e)
    {
        if (e.keyCode >= 60 && e.keyCode <= 90
            || (e.shiftKey && (e.keyCode >= 48 && e.keyCode <= 57))
            || (e.keyCode >= 106 && e.keyCode <= 111)
            || (e.keyCode >= 186 && e.keyCode <= 222)
            || e.keyCode == 12 || e.keyCode == 32
        ) {
            return false;
        }
    },

    Check: function(obj)
    {
        var value = obj.value;
        var newValue = value.replace(/[^0-9]/g, '');

        if (value != newValue) {
            obj.value = newValue;
        }
    }
};


$(document).ready(function(){
    FieldNumber.Init($('[data-numeric=true]'));
});